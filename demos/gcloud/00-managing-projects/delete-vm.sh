#!/bin/bash

cd `dirname $0`

#
# Source the environment script to pick up the variable definitions.
. ./env.sh

#
# Activate the configuration
gcloud config configurations activate $project

#
# Delete the instance. Use --quiet to avoid being prompted
gcloud compute instances delete $host --quiet