# This demo shows one way to manage gcloud projects.

Gcloud projects should be thought of as being as disposable as other resources, like virtual machines, and to use each project for one thing and one thing only. Google has [more about creating and deleting projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects) on their website.

Given you can have multiple projects, you need to be able to control which one you're managing or interacting with at any time. The easy way to do that is to create a [*gcloud configuration*](https://cloud.google.com/sdk/gcloud/reference/config/configurations) for each project, and simply switch from one project to the next by switching configurations. A configuration is simply a way of grouping a set of default values together, such as the project ID, the default region for that project, and so on. That can greatly simplify the number of options you have to put in when running gcloud commands, and help avoid mistakes such as using the wrong region etc.

This demo shows how to create a project with the *gcloud* CLI, how to create a configuration for it, and how to use that configuration when creating a virtual machine.

*Tip:* Create a configuration called *default*, set the project, region and zone to *none*. Then you can activate the default configuration and be sure you are not messing with a real project!