#!/bin/bash

cd `dirname $0`

set -e # Abort on error

#
# Source the environment script to pick up the variable definitions.
. ./env.sh

#
# I assume the project was created and a configuration for it too,
# so I can simply activate the configuration
gcloud config configurations activate $project

#
# I prefer CentOS. This is closer to the OS we run at EBI than most others,
# so is a good bet if you want some sort of compatibility.
#
# You can get a list of centos & rhel images with:
# gcloud compute images list --filter="family:(rhel,centos)"
#
# note that 'image_project' is a container for images. You have access to public
# 'image projects', as well as any images you've created in your own project.
image="centos-8-v20200618"
image_project="centos-cloud"

#
# This isn't strictly necessary, but illustrates how you can extract information
# about your project with the gcloud command. Here I get the computing service
# account email address.
service_account=`gcloud iam service-accounts list --format="value(email)"`

#
# Finally, create a VM!
#
# Use '--no-address' if you don't want to assign a public IP address
gcloud beta compute instances create $host \
  --project=$project \
  --zone=$zone \
  --machine-type=$instance \
  --service-account=$service_account \
  --subnet=default \
  --image=$image \
  --image-project=$image_project \
  --boot-disk-size=$disk_size \
  --boot-disk-device-name=$host

echo ' '
echo 'Listing all the VMs in your project...'
gcloud beta compute instances list
