# This shows how to use a VM in GCP to download your Coursera courses!

## Getting started
If you want to use a Google VM for this, start by copying the scripts from *../00-managing-projects* here, configure the *env.sh*, and create a project and a virtual machine.

SSH into the machine:
`gcloud compute ssh wildish-vm`

then install the coursera-dl tool:
`pip2 install --user coursera-dl`

Alternatively, you can install coursera-dl on your own laptop. It's up to you.

## Authenticating with coursera-dl
The tool currently seems to be buggy, the *-u* and *-p* options don't work well. The only way I found that works is to grab the *CAUTH* cookie from your browser and use that instead.

For Safari, go to the *Developer* -> *Show Web Inspector* menu item. Select the *Storage* tab, then *Cookies* on the left hand side.

Find the CAUTH cookie, copy it to a text file, and extract just the long cookie string, not all the cruft that comes with it.

If you're using another browser, please google how to do it for your browser, and provide instructions here.

In your VM, set an environment variable to hold the cookie:
`export cauth='very-long-cookie-string'`

*N.B.* Don't save the cookie to a file on your VM. That would effectively give your account credentials to anyone who can log into the VM, which isn't good

## Finding the course 'slug'
The 'slug' is the part of the URL in the browser that identifies the course. If the *--list-courses* option of coursera-dl were working, that would do it all for you automatically, but it isn't, so you have to do this manually.

Top find the slug, simply go into the course in the browser and look at the URL. For example, in the second course in the specialisation, *Essential Google Cloud Infrastructure: Foundation*, the URL is *https://www.coursera.org/learn/gcp-infrastructure-foundation/home/welcome*. The slug is the part that identifies the course, so *gcp-infrastructure-foundation*.

## Downloading the course
Now comes the easy part, just run this command:
```
coursera-dl --cauth $cauth --path download --subtitle-language en $slug
```

_et voila_, the course material appears magically under the *download* subdirectory!

Here's a list of the slugs for the ACE specialisation

Course title | slug
-------------|-----
Google Cloud Platform Fundamentals: Core Infrastructure | gcp-fundamentals
Essential Google Cloud Infrastructure: Foundation | gcp-infrastructure-foundation
Essential Google Cloud Infrastructure: Core Services | gcp-infrastructure-core-services
Elastic Google Cloud Infrastructure: Scaling and Automation | gcp-infrastructure-scaling-automation
Reliable Google Cloud Infrastructure: Design and Process | cloud-infrastructure-design-process
Preparing for the Google Cloud Professional Cloud Architect Exam | preparing-cloud-professional-cloud-architect-exam

So to get the lot, just copy and paste this code snippet:

```
for slug in \
  gcp-fundamentals \
  gcp-infrastructure-foundation \
  gcp-infrastructure-core-services \
  gcp-infrastructure-scaling-automation \
  cloud-infrastructure-design-process \
  preparing-cloud-professional-cloud-architect-exam
do
  coursera-dl --cauth $cauth --path download --subtitle-language en $slug
done
```

The whole thing comes to about 1.7 GB.